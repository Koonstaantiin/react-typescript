import { ClientError, GraphQLClient, Variables } from "graphql-request"
import { GRAPH_QL_FETCH_URL } from "../const"

const graphQLClient = new GraphQLClient(GRAPH_QL_FETCH_URL, {
  headers: {
    "x-api-key": process.env.REACT_APP_APP_SYNC_API_KEY || "",
  },
})

export const graphqlBaseQuery =
  () =>
  async ({ body, variables }: { body: string; variables?: Variables }) => {
    try {
      const result = await graphQLClient.request(body, variables)
      return { data: result }
    } catch (error) {
      if (error instanceof ClientError) {
        return { error: { status: error.response.status, data: error } }
      }
      return { error: { status: 500, data: error } }
    }
  }
