export const BASE_FETCH_URL = "https://jsonplaceholder.typicode.com/"
export const BASE_AWS_FETCH_URL = "https://vdy1f7os7f.execute-api.us-east-1.amazonaws.com/dev/"
export const GRAPH_QL_FETCH_ROOT_URL = "https://z35ekrl5pzfrdnhjaq3bx3zfsm.appsync-api.us-east-1.amazonaws.com"
export const GRAPH_QL_FETCH_URL = `${GRAPH_QL_FETCH_ROOT_URL}/graphql`
export const ROOT_ID = "root"
