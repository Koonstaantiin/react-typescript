export interface ViewProps<Data> {
  data?: Data
  closeModal?: () => void
}
