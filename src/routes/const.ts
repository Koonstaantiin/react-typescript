export const ROUTES_RELATIVE = {
  MAIN_PAGE: "",
  LOCATIONS_GRAPHQL: "locations",
  LOCATIONS_SUSPENSE: "locations-suspense",
  POSTS_RTK_QUERY: "posts",
  POST: "posts/:postId",
  USER_POSTS: "user-posts",
  LOGIN_FORM: "login-form",
  TABS: "tabs",
} as const
