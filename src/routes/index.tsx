import { Layout } from "@/UI/Layout"
import { Loader } from "@/UI/Loader"
import React from "react"
import { createBrowserRouter } from "react-router-dom"
import { ROUTES_RELATIVE } from "./const"

const Locations = React.lazy(() => import("@/modules/Locations"))
const Posts = React.lazy(() => import("@/modules/Posts"))
const Post = React.lazy(() => import("@/modules/Post"))
const UserPosts = React.lazy(() => import("@/modules/UserPosts"))
const ErrorPage = React.lazy(() => import("@/UI/ErrorPage/view"))
const LoginForm = React.lazy(() => import("@/modules/LoginForm"))
const Tabs = React.lazy(() => import("@/modules/Tabs"))

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout isPassedToWithAuthenticator />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: ROUTES_RELATIVE.LOCATIONS_GRAPHQL,
        async lazy() {
          const { Locations: LocationsLazy } = await import("../modules/Locations")
          return { Component: LocationsLazy }
        },
      },
      {
        path: ROUTES_RELATIVE.LOCATIONS_SUSPENSE,
        element: (
          <React.Suspense fallback={<Loader />}>
            <Locations />
          </React.Suspense>
        ),
      },
      {
        path: ROUTES_RELATIVE.POSTS_RTK_QUERY,
        element: (
          <React.Suspense fallback={<Loader />}>
            <Posts />
          </React.Suspense>
        ),
      },
      {
        path: ROUTES_RELATIVE.POST,
        element: (
          <React.Suspense fallback={<Loader />}>
            <Post />
          </React.Suspense>
        ),
      },
      {
        path: ROUTES_RELATIVE.USER_POSTS,
        element: (
          <React.Suspense fallback={<Loader />}>
            <UserPosts />
          </React.Suspense>
        ),
      },
      {
        path: ROUTES_RELATIVE.LOGIN_FORM,
        element: (
          <React.Suspense fallback={<Loader />}>
            <LoginForm />
          </React.Suspense>
        ),
      },
      {
        path: ROUTES_RELATIVE.TABS,
        element: (
          <React.Suspense fallback={<Loader />}>
            <Tabs />
          </React.Suspense>
        ),
      },
    ],
  },
])
