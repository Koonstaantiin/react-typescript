import { ROUTES_RELATIVE } from "./const"

export type ROUTES_RELATIVE = typeof ROUTES_RELATIVE
export type ROUTES_ABSOLUTE = {
  [K in keyof ROUTES_RELATIVE]: `/${ROUTES_RELATIVE[K]}`
}
