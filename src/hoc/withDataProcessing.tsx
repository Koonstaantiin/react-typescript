import { ErrorPage } from "@/UI/ErrorPage"
import { Loader } from "@/UI/Loader"
import { ApolloError } from "@apollo/client"
import { SerializedError } from "@reduxjs/toolkit"
import { FetchBaseQueryError } from "@reduxjs/toolkit/dist/query"

export const withDataProcessing =
  <P extends { data?: unknown }>(Component: React.FunctionComponent<P>, LoaderComponent = Loader) =>
  (
    props: P & {
      isLoading: boolean
      error: FetchBaseQueryError | SerializedError | ApolloError | undefined
    }
  ) => {
    const { isLoading, error, data, ...rest } = props

    if (isLoading) return <LoaderComponent />
    if (error && "data" in error) return <ErrorPage status={error.status} />
    if (!data) {
      return <>No data</>
    }

    return <Component data={data} {...(rest as unknown as P)} />
  }
