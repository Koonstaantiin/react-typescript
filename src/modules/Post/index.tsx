import { useParams } from "react-router-dom"
import { useGetPostByIdQuery } from "../../services/Post"
import { View } from "./view"

export const Post = () => {
  const { postId } = useParams<{ postId: string }>()
  if (!postId) {
    return <>No data for id #{postId}</>
  }
  const { isLoading, error, data } = useGetPostByIdQuery(postId)
  return <View isLoading={isLoading} data={data} error={error} />
}

export * from "../../services/Post"

export default Post
