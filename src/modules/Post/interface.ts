export interface Post {
  userId: number
  id: number
  title: string
  body: string
}

export type PostUpdate = Partial<Omit<Post, "id">>

export type PostCreate = Omit<Post, "id">
