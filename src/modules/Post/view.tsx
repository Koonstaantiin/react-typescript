import { withDataProcessing } from "@/hoc/withDataProcessing"
import { PostCommentModal } from "@/modals/PostComment"
import { ViewProps } from "@/shared/interfaces/common"
import Box from "@mui/material/Box"
import Button from "@mui/material/Button"
import Card from "@mui/material/Card"
import Typography from "@mui/material/Typography"
import { useState } from "react"
import { Link } from "react-router-dom"
import { Post } from "./interface"

const PostView = ({ data }: ViewProps<Post>) => {
  const { userId, title, body, id } = data || {}
  const [isOpen, setOpen] = useState(false)
  const openModal = () => setOpen(true)
  const closeModal = () => setOpen(false)
  return (
    <Box>
      <PostCommentModal postId={id?.toString()} isOpen={isOpen} closeModal={closeModal} />
      <Card variant="outlined">
        <Link to="/posts">
          <Button>Posts page</Button>
        </Link>
        <Button onClick={openModal}>Comments</Button>
        <Typography variant="h6">Post page (RTK Query + AWS configuration)</Typography>
        <Typography>id: {id}</Typography>
        <Typography>userId: {userId}</Typography>
        <Typography>title: {title}</Typography>
        <Typography>body: {body}</Typography>
      </Card>
    </Box>
  )
}

export const View = withDataProcessing(PostView)
