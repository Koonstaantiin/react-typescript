/* eslint-disable react/no-unused-prop-types */
import { useState, Children, cloneElement } from "react"
import { pathOr } from "ramda"

const Tab = ({ children, isActive }: { title?: string; children: React.ReactNode; isActive?: boolean }) => (
  <div
    style={{
      display: isActive ? "block" : "none",
    }}
  >
    {children}
  </div>
)

const TabSet = ({ children }: any) => {
  const [activeTab, setActiveTab] = useState(0)
  const handleTabChange = (tabIndex: number) => () => {
    setActiveTab(tabIndex)
  }
  return (
    <div>
      <div
        style={{
          borderBottom: "1px solid #000",
          padding: "5px 0",
        }}
      >
        {children.map((Component: any, index: number) => {
          const tabName = pathOr("", ["props", "title"])(Component)
          return (
            <button
              type="button"
              key={tabName}
              onClick={handleTabChange(index)}
              style={{
                padding: "0 5px",
                margin: "0 3px",
                fontSize: "12px",
                fontWeight: "800",
              }}
            >
              {tabName}
            </button>
          )
        })}
      </div>
      <div>
        {Children.map(children, (child, index) =>
          cloneElement(child, {
            key: `children-${child.props.title}`,
            isActive: activeTab === index,
          })
        )}
      </div>
    </div>
  )
}

const Tabs = () => (
  <TabSet>
    <Tab title="First">1st contents</Tab>
    <Tab title="Second">2nd contents</Tab>
    <Tab title="Third">3rd contents</Tab>
  </TabSet>
)

export default Tabs
