import { useState, useEffect } from "react"
import { API, graphqlOperation } from "aws-amplify"
import { pathOr } from "ramda"
import { listUsers } from "@/graphql/queries"
import { User } from "../interfaces"
// import { useQuery } from "@apollo/client"

export const useUsers = () => {
  const [users, setUsers] = useState<User[]>([])
  const [isLoading, setIsLoading] = useState(false)
  const [isError, setIsError] = useState(false)
  const [isSuccess, setIsSuccess] = useState(false)
  const [error, setError] = useState<unknown>()

  const fetchUsers = async () => {
    try {
      setIsError(false)
      setIsLoading(true)
      const usersData = await API.graphql(graphqlOperation(listUsers))
      const usersList = pathOr([], ["data", "listUsers", "items"])(usersData)
      setUsers(usersList)
      setIsSuccess(true)
    } catch (catchError) {
      setUsers([])
      setIsError(true)
      setError(catchError)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    fetchUsers()
  }, [])

  // const { data: users, ...rest } = useQuery(GET_USERS)
  return {
    users,
    error,
    isLoading,
    isError,
    isSuccess,
  }
}
