export interface User {
  id: number | string
  name: string
}

export type Users = User[]
