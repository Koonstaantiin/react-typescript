import Typography from "@mui/material/Typography"
import { Config as FinalFormConfig } from "final-form"
import { AddUserForm } from "./components/AddUserForm"
import { UsersList } from "./components/UsersList"

export const View = () => {
  const handleSubmit: FinalFormConfig["onSubmit"] = (values, form) => {
    console.log(">>>handleSubmit", {
      values,
      form,
    })
  }
  return (
    <>
      <Typography align="center" variant="h5">
        Users
      </Typography>
      <AddUserForm handleSubmit={handleSubmit} />
      <UsersList />
    </>
  )
}
