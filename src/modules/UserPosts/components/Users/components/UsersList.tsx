import config from "@/aws-exports"
import { useGetUsersQuery, useUpdateUserMutation } from "@/services"
import { API } from "@aws-amplify/api"
import DeleteIcon from "@mui/icons-material/Delete"
import Box from "@mui/material/Box"
import IconButton from "@mui/material/IconButton"
import Paper from "@mui/material/Paper"
import Typography from "@mui/material/Typography"

API.configure(config)

export const UsersList = () => {
  const { data: users, isError, isLoading, isSuccess } = useGetUsersQuery()
  const [updateUser] = useUpdateUserMutation()
  const handleUpdateUser = () => {
    updateUser({
      id: 1,
      name: "User 1 updated",
    })
  }

  return (
    <Box>
      {isLoading && <Paper variant="outlined">Loading users...</Paper>}
      {isError && <Paper variant="outlined">Oops! Error occured</Paper>}
      {!isLoading &&
        isSuccess &&
        users?.map((user) => (
          <Paper
            variant="outlined"
            key={user.id}
            sx={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
            }}
          >
            <Typography variant="caption">{user.name}</Typography>
            <IconButton aria-label="delete">
              <DeleteIcon />
            </IconButton>
          </Paper>
        ))}
      <IconButton aria-label="update" onClick={handleUpdateUser}>
        <DeleteIcon />
      </IconButton>
    </Box>
  )
}
