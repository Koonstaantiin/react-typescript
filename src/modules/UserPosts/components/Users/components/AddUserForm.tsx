import Box from "@mui/material/Box"
import IconButton from "@mui/material/IconButton"
import Paper from "@mui/material/Paper"
import AddButton from "@mui/icons-material/Add"
import { Config as FinalFormConfig } from "final-form"
import { TextField } from "mui-rff"
import { Form } from "react-final-form"

interface AddUserFormProps {
  handleSubmit: FinalFormConfig["onSubmit"]
}

// users path add to the store redux
export const AddUserForm = ({ handleSubmit }: AddUserFormProps) => (
  <Box
    sx={{
      display: "flex",
      flexDirection: "column",
    }}
  >
    <Form
      onSubmit={handleSubmit}
      render={({ handleSubmit: onSubmit }) => (
        <form onSubmit={onSubmit}>
          <Paper
            elevation={0}
            sx={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              padding: "0 10px",
            }}
          >
            <TextField name="user" label="User" variant="standard" />
            <IconButton aria-label="add">
              <AddButton />
            </IconButton>
          </Paper>
        </form>
      )}
    />
  </Box>
)
