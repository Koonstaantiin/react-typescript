import Box from "@mui/material/Box"
import Grid from "@mui/material/Grid"
import Paper from "@mui/material/Paper"
import { Users } from "./components/Users"

export const View = () => (
  <Box sx={{ flexGrow: 1 }}>
    <Grid container spacing={2}>
      <Grid item xs={3}>
        <Paper
          sx={{
            width: "100%",
            padding: "10px",
          }}
        >
          <Users />
        </Paper>
      </Grid>
      <Grid item xs={3}>
        <Paper
          sx={{
            height: 400,
            width: "100%",
            backgroundColor: (theme) => (theme.palette.mode === "dark" ? "#1A2027" : "#fff"),
          }}
        />
      </Grid>
      <Grid item xs={3}>
        <Paper
          sx={{
            height: 400,
            width: "100%",
            backgroundColor: (theme) => (theme.palette.mode === "dark" ? "#1A2027" : "#fff"),
          }}
        />
      </Grid>
      <Grid item xs={3}>
        <Paper
          sx={{
            height: 400,
            width: "100%",
            backgroundColor: (theme) => (theme.palette.mode === "dark" ? "#1A2027" : "#fff"),
          }}
        />
      </Grid>
    </Grid>
  </Box>
)
