import { Post } from "@/modules/Post/interface"

export type Posts = Array<Post>
