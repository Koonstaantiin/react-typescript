import { useState } from "react"
import Button from "@mui/material/Button"
import Card from "@mui/material/Card"
import Box from "@mui/material/Box"
import { PostCreateModal } from "@/modals/PostCreate"
import { useCreatePostMutation } from "@/services"

export const View = () => {
  const [isOpen, setOpen] = useState(false)
  const openModal = () => setOpen(true)
  const closeModal = () => setOpen(false)
  const [createPost] = useCreatePostMutation()
  const handleSubmit = (data: any) => {
    console.log(">>>submiting:", data)
    createPost(data)
  }
  return (
    <Box>
      <PostCreateModal isOpen={isOpen} closeModal={closeModal} handleSubmit={handleSubmit} />
      <Card variant="outlined">
        <Button onClick={openModal}>Create</Button>
      </Card>
    </Box>
  )
}
