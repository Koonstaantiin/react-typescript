import React from "react"
import { Link } from "react-router-dom"
import { TableVirtuoso } from "react-virtuoso"
import { Post } from "@/modules/Post/interface"
import { ViewProps } from "@/shared/interfaces/common"
import Paper from "@mui/material/Paper"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import { Posts } from "./interface"
import * as S from "./styled"
import { Header } from "./components/Header"

const TableComponents = {
  Scroller: React.forwardRef<HTMLDivElement>((props, ref) => <TableContainer component={Paper} {...props} ref={ref} />),
  Table: <P extends object>(props: P) => <Table {...props} style={{ borderCollapse: "separate" }} />,
  TableHead,
  TableRow,
  TableBody: React.forwardRef<HTMLTableSectionElement>((props, ref) => <TableBody {...props} ref={ref} />),
}

const HeaderContent = () => (
  <S.TableRowFullHeight>
    <TableCell width={150}>ID</TableCell>
    <TableCell width={150} align="center">
      User id
    </TableCell>
    <TableCell width={150} align="center">
      Title
    </TableCell>
    <TableCell align="center">Body</TableCell>
  </S.TableRowFullHeight>
)

const InnerItem = ({ item: { id, userId, title, body } }: { item: Post }) => (
  <>
    <TableCell scope="row">
      <Link to={`/posts/${id}`}>View post #{id}</Link>
    </TableCell>
    <TableCell align="left">{userId}</TableCell>
    <TableCell align="left">{title}</TableCell>
    <TableCell align="left">{body}</TableCell>
  </>
)

const ItemContent = (index: number, item: Post) => <InnerItem key={item.id} item={item} />

export const View = ({ data }: ViewProps<Posts>) => (
  <>
    <Header />
    <TableVirtuoso
      style={{ height: "100%" }}
      data={data}
      components={TableComponents}
      fixedHeaderContent={HeaderContent}
      itemContent={ItemContent}
    />
    <style>{`html, body, #root { height: 100% }`}</style>
  </>
)
