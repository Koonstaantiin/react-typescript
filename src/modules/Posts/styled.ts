import styled from "styled-components"
import TableRow from "@mui/material/TableRow"

export const TableRowFullHeight = styled(TableRow)`
  height: 100%;
  background: #fff;
`
