import { useState, useEffect } from "react"
import { withDataProcessing } from "@/hoc/withDataProcessing"
import { useGetPostsQuery } from "../../services/Posts"
import { View } from "./view"

const PostsViewWithLoading = withDataProcessing(View)

export const Posts = () => {
  const [limit, setLimit] = useState(10)
  const { isLoading, error, data } = useGetPostsQuery(limit)

  useEffect(() => {
    setTimeout(() => {
      setLimit(100)
    }, 5000)
  }, [])

  // TODO: передать все параметры результата usePostsGetQuery? или, дополнительно isError, ...
  return <PostsViewWithLoading isLoading={isLoading} data={data} error={error} />
}

export * from "../../services/Posts"

export default Posts
