import { ROUTES_RELATIVE } from "@/routes/const"

export const ROUTES = [
  {
    label: "Main page",
    href: "/",
  },
  {
    label: "Locations (GraphQL)",
    href: `/${ROUTES_RELATIVE.LOCATIONS_GRAPHQL}`,
  },
  {
    label: "Locations (GraphQL, Suspense)",
    href: `/${ROUTES_RELATIVE.LOCATIONS_SUSPENSE}`,
  },
  {
    label: "Posts (RTK Query)",
    href: `/${ROUTES_RELATIVE.POSTS_RTK_QUERY}`,
  },
  {
    label: "User Posts (aka Todo list)",
    href: `/${ROUTES_RELATIVE.USER_POSTS}`,
  },
  {
    label: "Login form",
    href: `/${ROUTES_RELATIVE.LOGIN_FORM}`,
  },
  {
    label: "Tabs",
    href: `/${ROUTES_RELATIVE.TABS}`,
  },
] as const
