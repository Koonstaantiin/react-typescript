import { ROUTES } from "./routes"

export type Routes = typeof ROUTES
export type Route = Routes[number]
