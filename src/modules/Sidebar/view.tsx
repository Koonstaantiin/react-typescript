import Divider from "@mui/material/Divider"
import Paper from "@mui/material/Paper"
import Typography from "@mui/material/Typography"
import { ThemeProvider } from "@mui/material/styles"
import { MousePosition } from "./components/MousePosition"
import { RouterList } from "./components/RouterList"
import { RouterItem } from "./components/RouterItem"
import { theme } from "./theme"

export const View = () => (
  <ThemeProvider theme={theme}>
    <Paper>
      <Typography variant="h4" component="h1" align="center">
        Navigation
      </Typography>
      <Divider />
      <RouterList renderRoute={RouterItem} />
      <Divider />
      <MousePosition />
    </Paper>
  </ThemeProvider>
)
