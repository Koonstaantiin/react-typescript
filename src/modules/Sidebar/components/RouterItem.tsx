import ArrowRightIcon from "@mui/icons-material/ArrowRight"
import ListItem from "@mui/material/ListItem"
import ListItemButton from "@mui/material/ListItemButton"
import ListItemIcon from "@mui/material/ListItemIcon"
import ListItemText from "@mui/material/ListItemText"
import type { Route } from "../interfaces"
import * as S from "../styled"

export const RouterItem = ({ label, href }: Route) => (
  <ListItem key={label} disablePadding>
    <ListItemButton disableGutters>
      <ListItemIcon>
        <ArrowRightIcon />
      </ListItemIcon>
      <ListItemText primary={<S.Link to={href}>{label}</S.Link>} />
    </ListItemButton>
  </ListItem>
)
