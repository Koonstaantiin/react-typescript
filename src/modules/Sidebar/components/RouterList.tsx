import List from "@mui/material/List"
import type { Route } from "../interfaces"
import { ROUTES } from "../routes"

export const RouterList = ({ renderRoute }: { renderRoute: (params: Route) => JSX.Element }) => (
  <List>{ROUTES.map(renderRoute)}</List>
)
