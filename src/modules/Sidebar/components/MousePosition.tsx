import { useDeferredMousePosition } from "@/hooks/useDeferredMousePosition"

export const MousePosition = () => {
  const { x, y } = useDeferredMousePosition(1000)
  return (
    <div>
      {x && (
        <>
          Last mouse position: {x}, {y}
        </>
      )}
    </div>
  )
}
