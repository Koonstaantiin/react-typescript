import { render, screen } from "@testing-library/react"
import { Sidebar } from "."
import { BrowserRouter } from "react-router-dom"

test("renders Navigation sidebar title", () => {
  render(
    <BrowserRouter>
      <Sidebar />
    </BrowserRouter>
  )
  const titleElement = screen.getByText(/Navigation/i)
  expect(titleElement).toBeInTheDocument()
})
