import { Link as LinkOriginal } from "react-router-dom"
import styled from "styled-components"

export const Link = styled(LinkOriginal)`
  display: inline-block;
  width: 100%;
`
