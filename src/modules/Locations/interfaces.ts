export interface Location {
  id: number
  name: string
  description: string
  photo: string
}

export type Locations = Array<Location>

export interface Data {
  locations: Locations
}
