import { useQuery } from "@apollo/client"
import { withDataProcessing } from "@/hoc/withDataProcessing"
import { View } from "./view"
import { Data } from "./interfaces"
import { GET_LOCATIONS } from "./query"

const LocationsViewWithLoading = withDataProcessing(View)

export const Locations = () => {
  const { loading, error, data } = useQuery<Data>(GET_LOCATIONS, {
    variables: {
      first: 10,
    },
  })

  return <LocationsViewWithLoading isLoading={loading} data={data?.locations} error={error} />
}

export default Locations
