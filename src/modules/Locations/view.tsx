import { ViewProps } from "@/shared/interfaces/common"
import Paper from "@mui/material/Paper"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import { Location, Locations } from "./interfaces"

const renderRow = ({ id, name, description, photo }: Location) => (
  <TableRow key={id} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
    <TableCell component="th" scope="row">
      {name}
    </TableCell>
    <TableCell align="right">{description}</TableCell>
    <TableCell align="right">
      <img width="200" height="125" alt="location-reference" src={`${photo}`} />
    </TableCell>
  </TableRow>
)

const LocationHeader = () => (
  <TableRow>
    <TableCell>Location name</TableCell>
    <TableCell align="right">Description</TableCell>
    <TableCell align="right">Image</TableCell>
  </TableRow>
)
const LocationBody = ({ data }: { data?: Locations }) => (data ? <>{data.map(renderRow)}</> : null)

export const View = ({ data }: ViewProps<Locations>) => (
  <TableContainer component={Paper}>
    <Table sx={{ minWidth: 650 }} size="small" aria-label="simple table">
      <TableHead>
        <LocationHeader />
      </TableHead>
      <TableBody>
        <LocationBody data={data} />
      </TableBody>
    </Table>
  </TableContainer>
)

export default View
