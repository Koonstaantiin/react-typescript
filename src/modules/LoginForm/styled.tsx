import Button from "@mui/material/Button"
import Box from "@mui/material/Box"
import Link from "@mui/material/Link"
import Stack from "@mui/material/Stack"
import GlobalStyles from "@mui/material/GlobalStyles"
import styled from "styled-components"

export const Container = styled.div`
  margin-top: 10px;
  height: 100%;
`

export const SliderImage = styled.img`
  align-self: center;
`

export const inputGlobalStyles = (
  <GlobalStyles
    styles={{
      "html, body": {
        height: "100%",
      },
      body: {
        display: "flex",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
      },
      "#root": {
        width: "100%",
        height: "100%",
      },
    }}
  />
)

export const SocialConnectButton = styled(Button)`
  &.MuiButtonBase-root {
    text-transform: none;
    color: #000;
    border-color: rgb(245, 247, 249);
    width: 205px;
    height: 60px;
    border-radius: 10px;
  }
`

export const SocialConnectButtonWrapper = styled(Box)`
  display: flex;
  justify-content: space-between;
`

export const SignUpWrapper = styled(Stack)`
  margin-top: 15px;
  font-size: 14px;
  column-gap: 10px;
  & .MuiTypography-root {
    font-size: 14px;
  }
`

export const ForgotPasswordLink = styled(Link)`
  &.MuiTypography-root {
    text-align: right;
    color: #157afe;
    font-weight: 800;
    text-decoration: none;
    font-size: 14px;
    margin: 5px 0 15px 0;
  }
`

export const SubmitButton = styled(Button)`
  &.MuiButtonBase-root {
    height: 46px;
    border-radius: 10px;
    background: #157afe;
    text-transform: none;
  }
`
