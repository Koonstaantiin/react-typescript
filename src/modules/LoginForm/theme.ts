import { createTheme } from "@mui/material/styles"

export const theme = createTheme({
  components: {
    MuiTextField: {
      styleOverrides: {
        root: {
          width: "100%",
          height: 45,
          "& .MuiInputBase-root": {
            background: "#f5f7f9",
            paddingLeft: 0,
            height: "100%",
            borderRadius: "10px",
            fontSize: 14,
            color: "#ababab",
            "& .MuiInputAdornment-root:not(.MuiInputAdornment-hiddenLabel)": {
              marginTop: "0!important",
            },
          },
          "& .MuiInputBase-input": {
            height: "100%",
            padding: 0,
          },
        },
      },
    },
  },
})
