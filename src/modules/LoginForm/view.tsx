import { ReactComponent as FacebookIcon } from "@/assets/icons/facebook.svg"
import { ReactComponent as GoogleIcon } from "@/assets/icons/google.svg"
import EnhancedEncryptionTwoToneIcon from "@mui/icons-material/EnhancedEncryptionTwoTone"
import MailIcon from "@mui/icons-material/Mail"
import VisibilityIcon from "@mui/icons-material/Visibility"
import Box from "@mui/material/Box"
import Divider from "@mui/material/Divider"
import Link from "@mui/material/Link"
import Typography from "@mui/material/Typography"
import { ThemeProvider } from "@mui/material/styles"
import { SubmitHandler, useForm } from "react-hook-form"
import { ColumnLeft, Item, TextFieldCustom, TextFieldIcon, Wrapper } from "./components"
import { Carousel } from "./components/Carousel"
import { ColumnRight, ColumnRightBottom, ColumnRightCentering } from "./components/ColumnRight"
import page1Image from "./images/page1.png"
import { Inputs } from "./interfaces"
import * as S from "./styled"
import { theme } from "./theme"

export const View = () => {
  const {
    // register,
    handleSubmit,
    control,
    // watch,
    // formState: { errors },
  } = useForm<Inputs>()
  const onSubmit: SubmitHandler<Inputs> = (data) => console.log(data)

  return (
    <ThemeProvider theme={theme}>
      {S.inputGlobalStyles}
      <Wrapper>
        <ColumnLeft>
          <Carousel>
            <Item>
              <S.SliderImage alt="SEO text" className="slider-image" src={page1Image} />
              <Typography
                variant="h4"
                sx={{
                  fontSize: 14,
                }}
              >
                Welcome back!
              </Typography>
              <Box>
                <Typography variant="body2">Start managing your finance faster and better</Typography>
                <Typography variant="body2">Start managing your finance faster and better</Typography>
              </Box>
            </Item>
            <Item>2</Item>
            <Item>3</Item>
            <Item>4</Item>
          </Carousel>
        </ColumnLeft>
        <ColumnRight>
          <ColumnRightCentering>
            <Typography variant="h4" alignSelf="flex-start">
              Welcome back!
            </Typography>
            <Typography variant="body2" alignSelf="flex-start" margin="10px 0 20px 0">
              Start managing your finance faster and better
            </Typography>
            <Box sx={{ display: "flex", flexDirection: "row", justifyContent: "center", width: "100%" }}>
              <form
                method="post"
                onSubmit={handleSubmit(onSubmit)}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  width: "100%",
                }}
              >
                <TextFieldCustom
                  name="login"
                  control={control}
                  placeholder="you@example.com"
                  InputProps={{
                    startAdornment: (
                      <TextFieldIcon position="start">
                        <MailIcon />
                      </TextFieldIcon>
                    ),
                  }}
                />
                <TextFieldCustom
                  name="password"
                  control={control}
                  placeholder="At least 8 characters"
                  type="password"
                  InputProps={{
                    startAdornment: (
                      <TextFieldIcon position="start">
                        <EnhancedEncryptionTwoToneIcon />
                      </TextFieldIcon>
                    ),
                    endAdornment: (
                      <TextFieldIcon
                        position="end"
                        sx={{
                          background: "none",
                          color: "#999",
                          padding: 0,
                          fontSize: 12,
                          "& .MuiSvgIcon-root": {
                            width: "20px",
                          },
                        }}
                      >
                        <VisibilityIcon />
                      </TextFieldIcon>
                    ),
                  }}
                />

                <S.ForgotPasswordLink href="https://accounts.google.com/signin/v2/usernamerecovery">
                  Forgot password
                </S.ForgotPasswordLink>
                <S.SubmitButton type="submit" variant="contained">
                  Login
                </S.SubmitButton>
                <Box>
                  <Divider
                    sx={{
                      margin: "25px 0",
                      color: "rgb(171, 171, 171);",
                    }}
                  >
                    or
                  </Divider>
                </Box>
                <S.SocialConnectButtonWrapper>
                  <S.SocialConnectButton
                    type="button"
                    variant="outlined"
                    startIcon={<GoogleIcon width={24} height={24} />}
                  >
                    Google
                  </S.SocialConnectButton>
                  <S.SocialConnectButton
                    type="button"
                    variant="outlined"
                    startIcon={<FacebookIcon width={24} height={24} />}
                  >
                    Facebook
                  </S.SocialConnectButton>
                </S.SocialConnectButtonWrapper>
                <S.SignUpWrapper flexDirection="row" justifyContent="center">
                  <Typography>Don&apos;t you have an account?</Typography>
                  <Link
                    sx={{
                      fontWeight: 800,
                      textDecoration: "none",
                    }}
                    href="https://google.com"
                  >
                    Sign Up
                  </Link>
                </S.SignUpWrapper>
              </form>
            </Box>
          </ColumnRightCentering>
          <ColumnRightBottom>&copy; 2023 All right reserved</ColumnRightBottom>
        </ColumnRight>
      </Wrapper>
    </ThemeProvider>
  )
}
