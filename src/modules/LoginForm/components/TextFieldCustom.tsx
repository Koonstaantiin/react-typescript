import TextField, { TextFieldProps } from "@mui/material/TextField"
import { Control, Controller } from "react-hook-form"
import { Inputs } from "../interfaces"

type ViewProps = {
  control?: Control<Inputs>
  name: keyof Inputs
} & TextFieldProps

export const TextFieldCustom = ({ control, name, InputProps, ...rest }: ViewProps) => (
  <Controller
    name={name}
    control={control}
    render={({ field }) => (
      <TextField
        {...field}
        variant="filled"
        sx={{
          my: "5px",
        }}
        InputProps={{
          disableUnderline: true,
          ...InputProps,
        }}
        {...rest}
      />
    )}
  />
)
