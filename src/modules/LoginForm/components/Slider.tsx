import Carousel from "react-material-ui-carousel"
import Typography from "@mui/material/Typography"
import Paper from "@mui/material/Paper"
import Button from "@mui/material/Button"

type Item = {
  id: number
  name: string
  description: string
}

const Item = (props: { item: Item }) => (
  <Paper>
    <Typography variant="h4">{props.item.name}</Typography>
    <Typography variant="body1">{props.item.description}</Typography>
    <Button className="CheckButton">Check it out!</Button>
  </Paper>
)

export const Slider = () => {
  const items = [
    {
      id: 1,
      name: "Random Name #1",
      description: "Probably the most random thing you have ever seen!",
    },
    {
      id: 2,
      name: "Random Name #2",
      description: "Hello World!",
    },
    {
      id: 3,
      name: "Random Name #3",
      description: "Hello World!",
    },
    {
      id: 4,
      name: "Random Name #4",
      description: "Hello World!",
    },
    {
      id: 5,
      name: "Random Name #5",
      description: "Hello World!",
    },
    {
      id: 6,
      name: "Random Name #6",
      description: "Hello World!",
    },
  ]

  return (
    <Carousel
      navButtonsWrapperProps={{
        style: {
          bottom: "0",
          top: "unset",
        },
      }}
      sx={{
        minHeight: "100%",
      }}
    >
      {items.map((item, i) => (
        <Item key={item.id} item={item} />
      ))}
    </Carousel>
  )
}
