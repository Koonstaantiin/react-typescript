import CarouselOriginal from "react-material-ui-carousel"
import ArrowBack from "@mui/icons-material/ArrowBackIos"
import ArrowForward from "@mui/icons-material/ArrowForwardIos"
import { CarouselProps } from "react-material-ui-carousel/dist/components/types"
import { createStyles, makeStyles } from "@mui/styles"

const useStyles: any = makeStyles(() =>
  createStyles({
    navButtonWrapper: {
      position: "absolute",
      zIndex: 1,
      right: 0,
      bottom: 0,
      top: "unset",
      height: "auto",
      "&:has(> button[aria-label=Previous])": {
        right: "auto",
        left: 0,
      },
      "& > button": {
        "&:hover": {
          background: "#000",
        },
        "&[aria-label=Next], &[aria-label=Previous]": {
          backgroundColor: "#157afe",
          opacity: 1,
          "&:hover": {
            backgroundColor: "#1565c0",
            opacity: "1!important",
          },
          "& > svg": {
            fill: "#FFF",
          },
        },
        "&[aria-label=Next]": {
          paddingRight: 12,
          paddingLeft: 12,
        },
        "&[aria-label=Previous]": {
          paddingLeft: 14,
        },
      },
    },
    indicatorIconButton: {
      "& > svg": {
        fill: "#157afe",
        transition: "fill 1s ease, border-width 1s ease",
      },
    },
    activeIndicatorIconButton: {
      "& > svg": {
        border: "1px solid #157afe",
        borderRadius: "50%",
        fill: "#1565c0",
      },
    },
  })
)

export const Carousel = ({ children }: CarouselProps) => {
  const classes = useStyles()
  return (
    <CarouselOriginal
      navButtonsWrapperProps={{
        className: classes.navButtonWrapper,
      }}
      indicatorIconButtonProps={{
        className: classes.indicatorIconButton,
      }}
      activeIndicatorIconButtonProps={{
        className: classes.activeIndicatorIconButton,
      }}
      indicatorContainerProps={{
        style: {
          position: "absolute",
          bottom: 0,
          padding: "8px 0",
        },
      }}
      autoPlay={false}
      navButtonsAlwaysVisible
      NextIcon={<ArrowForward />}
      PrevIcon={<ArrowBack />}
      sx={{
        minHeight: "100%",
        width: "100%",
        display: "flex",
        "& > div > div > div > div > div": {
          display: "flex",
          justifyContent: "center",
        },
        "& button[aria-label=Previous], & button[aria-label=Next]": {
          background: "none",
          "&:hover": {
            background: "none",
          },
          "& > svg": {
            fill: "#000",
            width: 14,
          },
        },
      }}
    >
      {children}
    </CarouselOriginal>
  )
}
