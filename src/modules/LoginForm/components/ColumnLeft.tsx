import Grid from "@mui/material/Grid"
import { PropsWithChildren } from "react"

export const ColumnLeft = ({ children }: PropsWithChildren) => (
  <Grid
    item
    md={6}
    xs={12}
    sx={{
      background: "#F5F7F9",
      display: "flex",
      justifyContent: "center",
      justifySelf: "flex-end",
      alignItems: "center",
    }}
  >
    {children}
  </Grid>
)
