import { PropsWithChildren } from "react"
import Box from "@mui/material/Box"
import Paper from "@mui/material/Paper"

export const Item = ({ children }: PropsWithChildren) => (
  <Paper
    sx={{
      background: "#F5F7F9",
      width: 500,
      display: "flex",
      justifyContent: "center",
      flexDirection: "column",
      boxShadow: 0,
    }}
  >
    <Box
      sx={{
        minHeight: 500,
        width: 400,
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        alignSelf: "center",
      }}
    >
      {children}
    </Box>
  </Paper>
)
