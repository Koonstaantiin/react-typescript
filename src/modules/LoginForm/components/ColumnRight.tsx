import Grid from "@mui/material/Grid"
import Paper from "@mui/material/Paper"
import { PropsWithChildren } from "react"

export const ColumnRight = ({ children }: PropsWithChildren) => (
  <Grid
    item
    md={6}
    xs={12}
    sx={{
      display: "flex",
      alignItems: "center",
      alignSelf: "center",
      flexDirection: "column",
      height: "100%",
      justifyContent: "space-between",
    }}
  >
    {children}
  </Grid>
)

export const ColumnRightCentering = ({ children }: PropsWithChildren) => (
  <Paper
    sx={{
      boxShadow: 0,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      width: "430px",
      textAlign: "center",
      height: "100%",
      justifyContent: "center",
    }}
  >
    <Paper
      sx={{
        display: "flex",
        flexDirection: "column",
        boxShadow: 0,
      }}
    >
      {children}
    </Paper>
  </Paper>
)

export const ColumnRightBottom = ({ children }: PropsWithChildren) => (
  <Paper
    sx={{
      boxShadow: 0,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      width: "430px",
      textAlign: "center",
      textTransform: "uppercase",
      fontSize: "14px",
    }}
  >
    {children}
  </Paper>
)
