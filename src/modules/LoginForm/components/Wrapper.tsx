import Grid from "@mui/material/Grid"
import { PropsWithChildren } from "react"

export const Wrapper = ({ children }: PropsWithChildren) => (
  <Grid
    container
    className="example"
    spacing={1}
    direction="row"
    sx={{
      justifyContent: "center",
      height: "100%",
    }}
  >
    {children}
  </Grid>
)
