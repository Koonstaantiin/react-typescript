import InputAdornment, { InputAdornmentTypeMap } from "@mui/material/InputAdornment"
import { PropsWithChildren } from "react"

export const TextFieldIcon = ({
  children,
  position,
  sx,
}: PropsWithChildren<{
  position: InputAdornmentTypeMap["props"]["position"]
  sx?: InputAdornmentTypeMap["props"]["sx"]
}>) => (
  <div
    style={{
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      padding: "0 5px",
    }}
  >
    <InputAdornment
      position={position}
      sx={{
        width: "100%",
        padding: "3px 6px",
        height: "100%",
        background: "#fefeff",
        color: "#157AFE",
        marginRight: 0,
        ...sx,
      }}
    >
      {children}
    </InputAdornment>
  </div>
)
