import { store } from "@/store"
import { ApolloClient, ApolloProvider, InMemoryCache, createHttpLink } from "@apollo/client"
import { Authenticator } from "@aws-amplify/ui-react"
import "@aws-amplify/ui-react/styles.css"
import { Amplify } from "aws-amplify"
import ReactDOM from "react-dom/client"
import { Provider as ReduxProvider } from "react-redux"
import { RouterProvider } from "react-router-dom"
import awsExports from "./aws-exports"
import "./index.css"
import { router } from "./routes"
import { GRAPH_QL_FETCH_URL, ROOT_ID } from "./shared/const"

const link = createHttpLink({
  uri: GRAPH_QL_FETCH_URL,
  credentials: "include",
})

const client = new ApolloClient({
  uri: GRAPH_QL_FETCH_URL,
  cache: new InMemoryCache(),
  link,
})

Amplify.configure(awsExports)

const root = ReactDOM.createRoot(document.getElementById(ROOT_ID) as HTMLElement)
root.render(
  <ReduxProvider store={store}>
    <ApolloProvider client={client}>
      <Authenticator.Provider>
        <RouterProvider router={router} />
      </Authenticator.Provider>
    </ApolloProvider>
  </ReduxProvider>
)
