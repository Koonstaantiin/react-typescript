import { ROOT_ID } from "@/shared/const"
import Modal from "react-modal"
import { ViewProps } from "./interface"
import { ContentStyles } from "./styled"

Modal.setAppElement(`#${ROOT_ID}`)

export const View = (props: ViewProps) => (
  <Modal
    {...props}
    style={{
      content: ContentStyles,
      overlay: {},
      ...props.style,
    }}
    contentLabel="Example Modal"
  >
    {props.children}
  </Modal>
)
