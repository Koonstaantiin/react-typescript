import { useRouteError } from "react-router-dom"
import { compose, join, filter, not, isNil, props, defaultTo } from "ramda"

export const View = ({ status = "" }: { status?: number | string }) => {
  const error = useRouteError()
  const message = compose(
    join(", "),
    filter(compose(not, isNil)),
    props(["statusText", "message"]),
    defaultTo({})
  )(error)

  return (
    <div id="error-page">
      <h1>Oops!</h1>
      <p>Sorry, an unexpected error has occurred.</p>
      {status && (
        <p>
          <i>Error: {status}</i>
        </p>
      )}
      <p>
        <i>{message}</i>
      </p>
    </div>
  )
}

export default View
