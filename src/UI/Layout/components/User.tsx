import { useAuthenticator } from "@aws-amplify/ui-react"
import Button from "@mui/material/Button"
import Typography from "@mui/material/Typography"
import Box from "@mui/material/Box"

export const User = () => {
  const { user, signOut } = useAuthenticator((context) => [context.user])
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
      }}
    >
      <Typography>Hello, {user.username}</Typography>
      <Button onClick={signOut}>Sign out</Button>
    </Box>
  )
}
