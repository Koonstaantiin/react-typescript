import { withAuthenticator } from "@aws-amplify/ui-react"
import { View } from "./view"

export const Layout = withAuthenticator(View)
