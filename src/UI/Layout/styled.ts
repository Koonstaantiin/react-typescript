import OriginalContainer from "@mui/material/Container"
import OriginalGrid from "@mui/material/Grid"
import styled from "styled-components"

export const Container = styled(OriginalContainer)`
  margin-top: 10px;
  height: 100%;
`

export const Grid = styled(OriginalGrid)<{
  $visible?: boolean
}>`
  height: 100%;
  ${({ $visible = true }) => !$visible && `display: none;`}
`
