import { Sidebar } from "@/modules/Sidebar"
import { ROUTES_RELATIVE } from "@/routes/const"
import { WithAuthenticatorProps } from "@aws-amplify/ui-react"
import "@fontsource/roboto/300.css"
import "@fontsource/roboto/400.css"
import "@fontsource/roboto/500.css"
import "@fontsource/roboto/700.css"
import { compose, equals, not, prop } from "ramda"
import { Outlet, useMatch } from "react-router-dom"
import { User } from "./components/User"
import * as S from "./styled"

interface LayoutProps extends WithAuthenticatorProps {
  isPassedToWithAuthenticator: boolean
}

export const View = ({ isPassedToWithAuthenticator }: LayoutProps) => {
  if (!isPassedToWithAuthenticator) {
    throw new Error(`isPassedToWithAuthenticator was not provided`)
  }
  const routeMatchParams = useMatch(ROUTES_RELATIVE.LOGIN_FORM)
  const isNotLoginPage = compose(not, equals(`/${ROUTES_RELATIVE.LOGIN_FORM}`), prop("pathnameBase"))(routeMatchParams)

  return (
    <S.Container maxWidth={false}>
      <S.Grid container spacing={2}>
        <S.Grid item width={250} $visible={isNotLoginPage}>
          <User />
          <Sidebar />
        </S.Grid>
        <S.Grid item xs>
          <Outlet />
        </S.Grid>
      </S.Grid>
    </S.Container>
  )
}
