import { useState, useEffect } from "react"

interface MousePosition {
  x: number
  y: number
}

export const useMousePosition = () => {
  const [position, setPosition] = useState<MousePosition>({} as MousePosition)

  useEffect(() => {
    const handleMouseMove = ({ clientX: x, clientY: y }: MouseEvent) => {
      setPosition({ x, y })
    }

    window.addEventListener("mousemove", handleMouseMove)

    return () => {
      window.removeEventListener("mousemove", handleMouseMove)
    }
  }, [])

  return position
}
