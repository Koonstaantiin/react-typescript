import { useMousePosition } from "./useMousePosition"
import { useDebounce } from "./useDebounceValue"

export const useDeferredMousePosition = (delay: number) => {
  const position = useMousePosition()
  return useDebounce(position, delay)
}
