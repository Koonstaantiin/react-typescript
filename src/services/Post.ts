import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import { Post, PostCreate, PostUpdate } from "@/modules/Post/interface"
import { BASE_AWS_FETCH_URL } from "@/shared/const"

export const postApi = createApi({
  reducerPath: "post",
  baseQuery: fetchBaseQuery({ baseUrl: BASE_AWS_FETCH_URL }),
  endpoints: (builder) => ({
    getPostById: builder.query<Post, string>({
      query: (id: string) => `posts/${id}`,
    }),
    updatePost: builder.mutation<
      Post,
      {
        id: number
        data: PostUpdate
      }
    >({
      query: ({ id, data }) => ({
        url: `posts/${id}`,
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      }),
    }),
    createPost: builder.mutation<PostCreate, Post>({
      query: (post) => ({
        url: "posts",
        method: "POST",
        body: JSON.stringify(post),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      }),
    }),
    deletePost: builder.mutation<Post, number>({
      query: (id) => ({
        url: `posts/${id}`,
        method: "DELETE",
      }),
    }),
  }),
})

export const { useCreatePostMutation, useDeletePostMutation, useUpdatePostMutation, useGetPostByIdQuery } = postApi
