import { UpdateUserMutationVariables } from "@/API"
import { updateUser } from "@/graphql/mutations"
import { User, Users } from "@/modules/UserPosts/components/Users/interfaces"
import { graphqlBaseQuery } from "@/shared/lib/graphqlBaseQuery"
import { createApi } from "@reduxjs/toolkit/query/react"
import { gql } from "graphql-request"
import { pathOr } from "ramda"
import { listUsers } from "../graphql/queries"

export const usersApi = createApi({
  reducerPath: "users",
  tagTypes: ["Users"],
  baseQuery: graphqlBaseQuery(),
  endpoints: (builder) => ({
    getUsers: builder.query<Users, void>({
      query: () => ({
        body: gql`
          ${listUsers}
        `,
      }),
      transformResponse: pathOr([], ["listUsers", "items"]),
      providesTags: ["Users"],
    }),
    updateUser: builder.mutation<Users, User>({
      query: ({ id, name }) =>
        <
          {
            body: string
            variables: {
              input: UpdateUserMutationVariables["input"]
              condition: UpdateUserMutationVariables["condition"]
            }
          }
        >{
          body: gql`
            ${updateUser}
          `,
          variables: {
            input: {
              id,
              name,
            },
            // condition: {
            //   name: {
            //     eq: name,
            //   },
            // },
          },
        },
      invalidatesTags: ["Users"],
    }),
    // addUser: builder.mutation<Users, User>({
    //   query: (user) => ({
    //     url: "users",
    //     method: "POST",
    //     body: user,
    //   }),
    //   invalidatesTags: ["Users"],
    // }),
    // updateUser: builder.mutation<User, User>({
    //   query: ({ id, ...rest }) => ({
    //     url: `users/${id}`,
    //     method: "POST",
    //     body: rest,
    //   }),
    //   invalidatesTags: ["Users"],
    // }),
    // deleteUser: builder.mutation<boolean, User>({
    //   query: (id) => ({
    //     url: `users/${id}`,
    //     method: "DELETE",
    //     body: id,
    //   }),
    //   invalidatesTags: ["Users"],
    // }),
  }),
})

export const { useGetUsersQuery, useUpdateUserMutation } = usersApi
