import { BASE_FETCH_URL } from "@/shared/const"
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import { PostComments } from "@/modals/PostComment/interface"

export const postCommentsApi = createApi({
  reducerPath: "postComments",
  baseQuery: fetchBaseQuery({ baseUrl: BASE_FETCH_URL }),
  endpoints: (builder) => ({
    getCommentsByPostId: builder.query<PostComments, string>({
      query: (id: string) => `posts/${id}/comments`,
    }),
  }),
})

export const { useGetCommentsByPostIdQuery } = postCommentsApi
