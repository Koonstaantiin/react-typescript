import { BASE_AWS_FETCH_URL } from "@/shared/const"
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import { Posts } from "../modules/Posts/interface"

export const postsApi = createApi({
  reducerPath: "posts",
  baseQuery: fetchBaseQuery({ baseUrl: BASE_AWS_FETCH_URL }),
  endpoints: (builder) => ({
    getPosts: builder.query<Posts, number>({
      query: (limit = 5) => ({
        url: "posts",
        params: {
          _limit: limit,
        },
      }),
    }),
  }),
})

export const { useGetPostsQuery } = postsApi
