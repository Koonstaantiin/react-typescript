import { combineReducers, configureStore } from "@reduxjs/toolkit"
import { postCommentsApi, postApi, postsApi, usersApi } from "@/services"

export const store = configureStore({
  reducer: combineReducers({
    [postApi.reducerPath]: postApi.reducer,
    [postsApi.reducerPath]: postsApi.reducer,
    [postCommentsApi.reducerPath]: postCommentsApi.reducer,
    [usersApi.reducerPath]: usersApi.reducer,
  }),
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      postApi.middleware,
      postsApi.middleware,
      postCommentsApi.middleware,
      usersApi.middleware
    ),
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
