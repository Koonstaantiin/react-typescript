import { ViewProps } from "@/shared/interfaces/common"
import Button from "@mui/material/Button"
import Paper from "@mui/material/Paper"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import TableFooter from "@mui/material/TableFooter"
import { PostComments } from "./interface"
import { PostCommentItem } from "./components/PostCommentItem"

const PostCommentHead = () => (
  <TableRow>
    <TableCell>ID</TableCell>
    <TableCell>Post ID</TableCell>
    <TableCell>Name</TableCell>
    <TableCell>Email</TableCell>
    <TableCell>Body</TableCell>
  </TableRow>
)

const PostCommentBody = ({ data = [] }: { data?: PostComments }) => (
  <>
    {data.map((item) => (
      <PostCommentItem key={item.id} data={item} />
    ))}
  </>
)

const PostCommentsFooter = ({ closeModal }: Pick<ViewProps<PostComments>, "closeModal">) => (
  <TableRow>
    <TableCell colSpan={5}>
      <Button onClick={closeModal}>Close</Button>
    </TableCell>
  </TableRow>
)

export const View = ({ data, closeModal }: ViewProps<PostComments>) => (
  <TableContainer component={Paper}>
    <Table style={{ borderCollapse: "collapse" }}>
      <TableHead>
        <PostCommentHead />
      </TableHead>
      <TableBody>
        <PostCommentBody data={data} />
      </TableBody>
      <TableFooter>
        <PostCommentsFooter closeModal={closeModal} />
      </TableFooter>
    </Table>
  </TableContainer>
)

export default View
