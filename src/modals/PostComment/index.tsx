import { CommonModal } from "@/UI/CommonModal"
import { withDataProcessing } from "@/hoc/withDataProcessing"
import { useEffect, useState } from "react"
import { useGetCommentsByPostIdQuery } from "../../services/PostComments"
import { ModalParams } from "./interface"
import { View } from "./view"

const ViewWithLoading = withDataProcessing(View)

const Modal = ({ postId, isOpen, closeModal }: ModalParams) => {
  const [skip, setSkip] = useState(true)
  useEffect(() => {
    if (isOpen && skip) {
      setSkip(false)
    }
  }, [isOpen])
  const { isLoading, error, data } = useGetCommentsByPostIdQuery(postId || "", {
    skip,
  })
  return (
    <CommonModal isOpen={isOpen} onRequestClose={closeModal}>
      {postId ? <span /> : <span>No data for id #{postId}</span>}
      <ViewWithLoading isLoading={isLoading} data={data} error={error} closeModal={closeModal} />
    </CommonModal>
  )
}

export const PostCommentModal = Modal

export * from "../../services/PostComments"
