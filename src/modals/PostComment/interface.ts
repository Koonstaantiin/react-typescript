export interface PostComment {
  postId: number
  id: number
  name: string
  email: string
  body: string
}

export type PostComments = Array<PostComment>

export interface ModalParams {
  postId?: string
  isOpen: boolean
  closeModal: () => void
}
