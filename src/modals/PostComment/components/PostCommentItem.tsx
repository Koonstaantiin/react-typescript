import { styled } from "@mui/material"
import TableCell from "@mui/material/TableCell"
import TableRow from "@mui/material/TableRow"
import { useCallback, useState } from "react"
import { PostComment } from "../interface"

const StyledTableRow = styled(TableRow)`
  ${({ selected }) =>
    selected &&
    `
    background: #ddd;
  `}
`

export const PostCommentItem: React.FunctionComponent<{ data: PostComment }> = ({ data }) => {
  const [currentSelected, setCurrentSelected] = useState(false)
  const handleClick = useCallback(() => setCurrentSelected((prevState) => !prevState), [])
  const { id, postId, name, email, body } = data
  return (
    <StyledTableRow key={id} onClick={handleClick} selected={currentSelected}>
      <TableCell>{id}</TableCell>
      <TableCell>{postId}</TableCell>
      <TableCell>{name}</TableCell>
      <TableCell>{email}</TableCell>
      <TableCell>{body}</TableCell>
    </StyledTableRow>
  )
}
