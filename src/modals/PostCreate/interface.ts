import { Config as FinalFormConfig } from "final-form"

export interface ModalParams {
  isOpen: boolean
  closeModal: () => void
  handleSubmit: FinalFormConfig["onSubmit"]
}
