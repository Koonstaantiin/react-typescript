import styled from "styled-components"
import Box from "@mui/material/Box"

export const BoxStyled = styled(Box)`
  display: flex;
  flex-direction: column;

  & .MuiTextField-root {
    margin: 8px;
    width: 225px;
  }
`
