import { CommonModal } from "@/UI/CommonModal"
import AddButton from "@mui/icons-material/Add"
import IconButton from "@mui/material/IconButton"
import { TextField } from "mui-rff"
import { Form } from "react-final-form"
import { ModalParams } from "./interface"
import * as S from "./styled"

export const View = ({ isOpen, closeModal, handleSubmit }: ModalParams) => (
  <CommonModal isOpen={isOpen} onRequestClose={closeModal}>
    <S.BoxStyled>
      <Form
        onSubmit={handleSubmit}
        render={({ submitting, pristine, handleSubmit: onSubmit }) => (
          <form onSubmit={onSubmit}>
            <TextField name="userId" label="User ID" inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }} />
            <TextField name="title" label="Title" />
            <TextField name="body" label="Body" multiline />
            <IconButton aria-label="add" disabled={submitting || pristine} type="submit">
              <AddButton />
            </IconButton>
          </form>
        )}
      />
    </S.BoxStyled>
  </CommonModal>
)

export default View
