# react-typescript

React test repository

#### Used technologies:

- GIT
- Yarn
- CRACO (Create React App Configuration Override)
- Webpack
  - aliases
- React
  - Lazy loading
  - Suspense
  - Hook
  - HOC
  - Render props
- React Router
- React Modal
- React Virtualized (Virtuoso)
- Redux
- Redux Toolkit
  - RTK Query
- Typescript
  - Interfaces
  - Generics
  - etc.
- GraphQL
- AWS Amplify
  - Authentication
  - Hosting
  - Data model (User, Post, Comment)
- NodeJS (AWS Function, routing handle)
- MUI
- Testing library
- Ramda
- Fontsource Roboto
- Styled Components
- ESLint
- Prettier
