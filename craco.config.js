const path = require("path")

module.exports = {
  webpack: {
    alias: {
      "@": path.resolve(__dirname, "src"),
    },
  },
  jest: {
    configure: (jestConfig) => {
      return {
        ...jestConfig,
        preset: "ts-jest",
        testEnvironment: "node",
        rootDir: ".",
        transform: {
          "^.+\\.tsx?$": "ts-jest",
        },
        moduleNameMapper: {
          "^@/(.*)$": "$1",
        },
      }
    },
  },
}
